
// // IMPORTANT: Start your code on the 2nd line
/**
* BENNU.JS FILE
* This file is just a helper for the understanding
* and easy to read configs elements from Bennu
* WHY: A .cfg file is just a Javascript file that contains special
* elements from the configurations.
* From Mozilla:
* Although the extension of an AutoConfig file is typically .cfg, the AutoConfig file is a JavaScript file.
* This allows for additional JavaScript to be written within the file to add different logic in different situations.
* URL: https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
*
* So thats why this file exists: the main editors support javascript text highlighting out of the box
* but no normally .cfg files. Also all the forges such as Gitlab or Codeberg show sintax with js but
* no with .cfg files. So the idea is the next:
*  1.- Create a bennu.js file that could be written by almost everything
*  2.- Copy it into bennu.cfg
*  3.- Move new bennu.cfg file to directory
*  4.- NOW YOU HAVE FIREFOX CONFIGURED, Good.
*
* License: MPL v2.0
* Version: 135.0.1-2
*/

//------------- PHOENIX PART --------------//
lockPref("toolkit.telemetry.pioneer-new-studies-available", false);

/* Origin trials
[NOTE]: Origin Trials are a way for developers to test and use experimental web platform
features for a limited amount of time in exchange for feedback. Seems legit.
*/
// lockPref("dom.origin-trials.enabled", false);

/* Crash Reporting
  Socorro (spanish word for help) is the in-house crash statistics
  Is open source:
  https://github.com/mozilla-services/socorro
  https://wiki.mozilla.org/Socorro
  https://firefox-source-docs.mozilla.org/tools/sanitizer/asan_nightly.html
  https://github.com/choller/firefox-asan-reporter
  We already disable the telemetry so... this is just an extra */
lockPref("asanreporter.apiurl", "");
lockPref("asanreporter.clientid", "");
defaultPref("asanreporter.loglevel", 70);
lockPref("breakpad.reportURL", "");
lockPref("toolkit.crashreporter.include_context_heap", false); // Defense in depth

/* Misc. Telemetry
  https://mozilla.github.io/policy-templates/#disabletelemetry 
  https://mozilla.github.io/policy-templates/#firefoxsuggest
  https://searchfox.org/mozilla-central/source/testing/geckodriver/src/prefs.rs
  https://wiki.mozilla.org/QA/Telemetry
  https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/internals/preferences.html 
  https://searchfox.org/mozilla-central/source/modules/libpref/init/StaticPrefList.yaml
  https://searchfox.org/mozilla-central/source/remote/shared/RecommendedPreferences.sys.mjs
  https://searchfox.org/mozilla-central/source/testing/profiles/perf/user.js

Most of this settings are covered in Arkenfox and Policies*/
lockPref("browser.places.interactions.enabled", false); // https://searchfox.org/mozilla-central/source/browser/app/profile/firefox.js
lockPref("datareporting.healthreport.documentServerURI", ""); // [HIDDEN]
defaultPref("datareporting.healthreport.logging.consoleEnabled", false); // [HIDDEN]
lockPref("datareporting.healthreport.service.enabled", false); // [HIDDEN]
lockPref("datareporting.healthreport.service.firstRun", false); // [HIDDEN]
lockPref("datareporting.healthreport.uploadEnabled", false);
lockPref("datareporting.policy.dataSubmissionEnabled", false);
lockPref("datareporting.policy.dataSubmissionPolicyAccepted", false);
lockPref("datareporting.policy.dataSubmissionPolicyBypassNotification", true);
lockPref("datareporting.policy.firstRunURL", "");
lockPref("dom.security.unexpected_system_load_telemetry_enabled", false);
lockPref("network.jar.record_failure_reason", false); // https://searchfox.org/mozilla-release/source/modules/libpref/init/StaticPrefList.yaml#14397
lockPref("network.traffic_analyzer.enabled", false); // https://searchfox.org/mozilla-release/source/modules/libpref/init/StaticPrefList.yaml#13191
lockPref("network.trr.confirmation_telemetry_enabled", false);
lockPref("privacy.imageInputTelemetry.enableTestMode", false); // [HIDDEN] "Event Telemetry" https://searchfox.org/mozilla-central/source/modules/libpref/init/StaticPrefList.yaml#15549
lockPref("privacy.trackingprotection.emailtracking.data_collection.enabled", false);
lockPref("toolkit.content-background-hang-monitor.disabled", true); // BHR https://searchfox.org/mozilla-central/source/modules/libpref/init/StaticPrefList.yaml#16720
lockPref("toolkit.telemetry.archive.enabled", false);
lockPref("toolkit.telemetry.bhrPing.enabled", false);
lockPref("toolkit.telemetry.dap.helper.hpke", "");
lockPref("toolkit.telemetry.dap.helper.url", "");
lockPref("toolkit.telemetry.dap.leader.hpke", "");
lockPref("toolkit.telemetry.dap.leader.url", "");
defaultPref("toolkit.telemetry.dap.logLevel", "Off");
lockPref("toolkit.telemetry.dap_helper", "");
lockPref("toolkit.telemetry.dap_helper_owner", "");
lockPref("toolkit.telemetry.dap_leader", "");
lockPref("toolkit.telemetry.dap_leader_owner", "");
lockPref("toolkit.telemetry.firstShutdownPing.enabled", false);
lockPref("toolkit.telemetry.healthping.enabled", false); // [HIDDEN]
lockPref("toolkit.telemetry.newProfilePing.enabled", false);
lockPref("toolkit.telemetry.pioneerId", ""); // [HIDDEN]
lockPref("toolkit.telemetry.previousBuildID", "");
lockPref("toolkit.telemetry.reportingpolicy.firstRun", false);
lockPref("toolkit.telemetry.server", "data;");
lockPref("toolkit.telemetry.server_owner", "");
lockPref("toolkit.telemetry.shutdownPingSender.enabled", false);
lockPref("toolkit.telemetry.testing.suppressPingsender", true); // [HIDDEN]
defaultPref("toolkit.telemetry.translations.logLevel", "Off");
lockPref("toolkit.telemetry.unified", false);
lockPref("toolkit.telemetry.updatePing.enabled", false);
defaultPref("toolkit.telemetry.user_characteristics_ping.logLevel", "Off");
lockPref("toolkit.telemetry.user_characteristics_ping.opt-out", true);

// Misc. UX - Harmless but good add
defaultPref("datareporting.healthreport.infoURL", "");
defaultPref("extensions.recommendations.privacyPolicyUrl", "");
defaultPref("toolkit.crashreporter.infoURL", "");
defaultPref("toolkit.datacollection.infoURL", "");

// Firefox Recommendations & "Discovery"
// defaultPref("extensions.getAddons.browseAddons", ""); // [HIDDEN - non-Android] Arkenfox
// defaultPref("extensions.getAddons.showPane", false); // Arkenfox
defaultPref("extensions.htmlaboutaddons.recommendations.enabled", false);
defaultPref("extensions.recommendations.themeRecommendationUrl", "");
defaultPref("extensions.webservice.discoverURL", ""); // [HIDDEN - non-Thunderbird]

// Fakespot
defaultPref("toolkit.shopping.ohttpConfigURL", "");
defaultPref("toolkit.shopping.ohttpRelayURL", "");

// Firefox Relay
defaultPref("signon.firefoxRelay.feature", "disabled"); // [HIDDEN - Thunderbird]

// Remove Mozilla URL tracking params
// Example: original download release channel:
// https://www.mozilla.org/firefox/download/thanks/?s=direct&utm_medium=firefox-desktop&utm_source=backup&utm_campaign=firefox-backup-2024&utm_content=control
defaultPref("browser.backup.template.fallback-download.aurora", "https://www.mozilla.org/firefox/channel/desktop/#developer");
defaultPref("browser.backup.template.fallback-download.beta", "https://www.mozilla.org/firefox/channel/desktop/#beta");
defaultPref("browser.backup.template.fallback-download.esr", "https://www.mozilla.org/firefox/enterprise/#download");
defaultPref("browser.backup.template.fallback-download.nightly", "https://www.mozilla.org/firefox/channel/desktop/#nightly");
defaultPref("browser.backup.template.fallback-download.release", "https://www.mozilla.org/firefox/download/thanks/?s=direct");
defaultPref("signon.firefoxRelay.manage_url", "https://relay.firefox.com/accounts/profile/");

// Disable Network Prefetching
// https://developer.mozilla.org/docs/Glossary/Prefetch
defaultPref("dom.prefetch_dns_for_anchor_http_document", false); // https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42684
// defaultPref("dom.prefetch_dns_for_anchor_https_document", false); // [DEFAULT] https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42684
defaultPref("network.dns.disablePrefetch", true);
defaultPref("network.dns.disablePrefetchFromHTTPS", true);
// defaultPref("network.dns.prefetch_via_proxy", false); // [DEFAULT]
defaultPref("network.http.speculative-parallel-limit", 0); // [DEFAULT - Thunderbird]
// defaultPref("network.predictor.enable-hover-on-ssl", false); // [DEFAULT]
// defaultPref("network.predictor.enable-prefetch", false); // [DEFAULT]
defaultPref("network.predictor.enabled", false);
// defaultPref("network.prefetch-next", false); Arkenfox

/* Disable Preconnect
  https://github.com/uBlockOrigin/uBlock-issues/issues/2913
  https://developer.mozilla.org/docs/Web/HTML/Attributes/rel/preconnect
  https://bugzilla.mozilla.org/show_bug.cgi?id=1861889 */
defaultPref("network.preconnect", false);

/// Disable Early Hints
// https://developer.mozilla.org/docs/Web/HTTP/Status/103
// https://github.com/bashi/early-hints-explainer/blob/main/explainer.md
// Ex. like Cromite https://github.com/uazo/cromite/blob/master/build/patches/Client-hints-overrides.patch

defaultPref("network.early-hints.enabled", false);
defaultPref("network.early-hints.over-http-v1-1.enabled", false);
defaultPref("network.early-hints.preconnect.enabled", false);
defaultPref("network.early-hints.preconnect.max_connections", 0);

/* Ensure cyphered connections using HTTPS
   Very needed for security */
defaultPref("dom.security.https_first", true);
defaultPref("dom.security.https_first_for_custom_ports", true); // [DEFAULT, DEFENSE IN DEPTH]
// defaultPref("dom.security.https_first_pbm", true); // [DEFAULT]
defaultPref("dom.security.https_first_schemeless", true);
lockPref("dom.security.https_only_mode", true);
defaultPref("dom.security.https_only_mode.upgrade_local", true);
lockPref("dom.security.https_only_mode_pbm", true);
defaultPref("security.mixed_content.block_active_content", true);
defaultPref("security.mixed_content.block_display_content", true);
defaultPref("security.mixed_content.block_object_subrequest", true);
defaultPref("security.mixed_content.upgrade_display_content", true);
// defaultPref("security.mixed_content.upgrade_display_content.audio", true); // [DEFAULT]
// defaultPref("security.mixed_content.upgrade_display_content.image", true); // [DEFAULT]
// defaultPref("security.mixed_content.upgrade_display_content.video", true); // [DEFAULT]

/// Prevent sending HTTP requests to websites that do not respond quickly to check if they support HTTPS
// defaultPref("dom.security.https_only_mode_send_http_background_request", false); // Arkenfox

/* Show suggestions when an HTTPS page can not be found
   Ex. If 'example.com' isn't secure, it may suggest 'www.example.com' */
defaultPref("dom.security.https_only_mode_error_page_user_suggestions", true);

// Always warn on insecure webpages
defaultPref("security.insecure_connection_text.enabled", true);
defaultPref("security.insecure_connection_text.pbmode.enabled", true);
// defaultPref("security.ssl.treat_unsafe_negotiation_as_broken", true); Arkenfox
// defaultPref("security.warn_submit_secure_to_insecure", true); // [DEFAULT] Warn when submitting a form from HTTP to HTTPS

// Disable TLS 1.3 0-RTT (Not forward secret)
// https://github.com/tlswg/tls13-spec/issues/1001
defaultPref("network.http.http3.enable_0rtt", false); // For HTTP3 https://bugzilla.mozilla.org/show_bug.cgi?id=1689550
// defaultPref("security.tls.enable_0rtt_data", false); arkenfox

// Skip DoH Connectivity Checks
defaultPref("network.connectivity-service.DNS_HTTPS.domain", "");
defaultPref("network.trr.confirmationNS", "skip");

// Always warn before falling back from DoH to native DNS...
defaultPref("network.trr.display_fallback_warning", true);
defaultPref("network.trr_ui.show_fallback_warning_option", true);
defaultPref("network.trr.strict_native_fallback", true); // https://searchfox.org/mozilla-central/source/toolkit/components/telemetry/docs/data/environment.rst#438

// Fix IPv6 connectivity when DoH is enabled
// https://codeberg.org/divested/brace/pulls/5
defaultPref("network.dns.preferIPv6", true);

/* Enable Safe Browsing by default
   This won't do anything if you don't have an API key from Google, though doesn't hurt...
   Harmless from a privacy perspective due to the below changes, also effective at preventing real-time malicious domains and downloads.
   We will of course **ALWAYS** give users the ability to disable. */ /*
defaultPref("browser.safebrowsing.blockedURIs.enabled", true); // [DEFAULT]
defaultPref("browser.safebrowsing.downloads.enabled", true); // [DEFAULT - non-Android]
defaultPref("browser.safebrowsing.downloads.remote.url", "https://sb-ssl.google.com/safebrowsing/clientreport/download?key=%GOOGLE_SAFEBROWSING_API_KEY%"); // [DEFAULT]
defaultPref("browser.safebrowsing.features.blockedURIs.update", true); // [DEFAULT, HIDDEN] https://searchfox.org/mozilla-central/source/toolkit/components/url-classifier/SafeBrowsing.sys.mjs
defaultPref("browser.safebrowsing.features.downloads.update", true); // [DEFAULT, HIDDEN] https://searchfox.org/mozilla-central/source/toolkit/components/url-classifier/SafeBrowsing.sys.mjs
defaultPref("browser.safebrowsing.features.malware.update", true); // [DEFAULT, HIDDEN - non-Android] https://searchfox.org/mozilla-central/source/toolkit/components/url-classifier/SafeBrowsing.sys.mjs
defaultPref("browser.safebrowsing.features.phishing.update", true); // [DEFAULT, HIDDEN - non-Android] https://searchfox.org/mozilla-central/source/toolkit/components/url-classifier/SafeBrowsing.sys.mjs
defaultPref("browser.safebrowsing.malware.enabled", true); // [DEFAULT]
defaultPref("browser.safebrowsing.phishing.enabled", true); // [DEFAULT]
defaultPref("browser.safebrowsing.update.enabled", true); // [DEFAULT, HIDDEN] https://searchfox.org/mozilla-central/source/toolkit/components/url-classifier/SafeBrowsing.sys.mjs
defaultPref("urlclassifier.downloadAllowTable", "goog-downloadwhite-proto"); // [DEFAULT - non-Android]
defaultPref("urlclassifier.downloadBlockTable", "goog-badbinurl-proto"); // [DEFAULT - non-Android] */

/* Disable the legacy Safe Browsing API (v2.2...)
  https://code.google.com/archive/p/google-safe-browsing/wikis/Protocolv2Spec.wiki
  Has been nonfunctional since October 2018
  https://security.googleblog.com/2018/01/announcing-turndown-of-deprecated.html
  Let's make sure it's not used for defense in depth (and attack surface reduction...) */
defaultPref("browser.safebrowsing.provider.google.advisoryName", "Google Safe Browsing (Legacy)"); // Label it so it's clearly distinguishable if it is ever enabled for whatever reason...
defaultPref("browser.safebrowsing.provider.google.gethashURL", "");
defaultPref("browser.safebrowsing.provider.google.updateURL", "");

/* Proxy Safe Browsing
  These are using the servers of IronFox, hosted on a Cloudflare storage bucket (in EU jurisdiction) */
defaultPref("browser.safebrowsing.provider.google4.gethashURL", "https://safebrowsing.ironfoxoss.org/v4/fullHashes:find?$ct=application/x-protobuf&key=%GOOGLE_SAFEBROWSING_API_KEY%&$httpMethod=POST");
defaultPref("browser.safebrowsing.provider.google4.updateURL", "https://safebrowsing.ironfoxoss.org/v4/threatListUpdates:fetch?$ct=application/x-protobuf&key=%GOOGLE_SAFEBROWSING_API_KEY%&$httpMethod=POST");

/* Enforce that no data is shared with Google
  https://bugzilla.mozilla.org/show_bug.cgi?id=1351147 */
// lockPref("browser.safebrowsing.provider.google.dataSharing.enabled", false); // [DEFAULT, HIDDEN - non-Android]
// lockPref("browser.safebrowsing.provider.google4.dataSharing.enabled", false); // [DEFAULT]
lockPref("browser.safebrowsing.provider.google4.dataSharingURL", "");

/* By default, when you report a Safe Browsing false positive, it sends the URL to both Mozilla & Google (NOT PROXIED), as well as your locale to Mozilla
Ex. https://en-us.phish-error.mozilla.com/?url=example.org - Which redirects you directly to https://safebrowsing.google.com/safebrowsing/report_error/?tpl=mozilla&url=example.org
We can improve privacy & speed by sending the domain *only* to Google & without sending your locale to anyone
We could also potentially strip tpl=mozilla which tells Google the request is from Firefox - though it looks like there is a different page for Firefox users with a better privacy policy, so we will leave it for now
Unclear whether 'MalwareMistake' is used, but we can set it anyways */
defaultPref("browser.safebrowsing.provider.google.reportMalwareMistakeURL", "https://safebrowsing.google.com/safebrowsing/report_error/?tpl=mozilla&url=");
defaultPref("browser.safebrowsing.provider.google.reportPhishMistakeURL", "https://safebrowsing.google.com/safebrowsing/report_error/?tpl=mozilla&url=");
defaultPref("browser.safebrowsing.provider.google4.reportMalwareMistakeURL", "https://safebrowsing.google.com/safebrowsing/report_error/?tpl=mozilla&url=");
defaultPref("browser.safebrowsing.provider.google4.reportPhishMistakeURL", "https://safebrowsing.google.com/safebrowsing/report_error/?tpl=mozilla&url=");

// Similar behavior also appears to happen when you report a URL to Safe Browsing
defaultPref("browser.safebrowsing.reportPhishURL", "https://safebrowsing.google.com/safebrowsing/report_phish/?tpl=mozilla&url=");

/* Unclear whether these are actually used or not, but looks like Firefox has some kind of functionality to view a "report" from Safe Browsing about the safety, history, & general status of a site
By default, it unnecessarily redirects from ex. https://safebrowsing.google.com/safebrowsing/diagnostic?site=example.org to https://transparencyreport.google.com/safe-browsing/search?url=example.org
We can skip the redirect to improve speed */
defaultPref("browser.safebrowsing.provider.google.reportURL", "https://transparencyreport.google.com/safe-browsing/search?url=");
defaultPref("browser.safebrowsing.provider.google4.reportURL", "https://transparencyreport.google.com/safe-browsing/search?url=");

// Prevent Wifi scanning
defaultPref("geo.wifi.scan", false); // [HIDDEN] https://searchfox.org/mozilla-release/source/remote/shared/RecommendedPreferences.sys.mjs#299

// Set BeaconDB as the network Geolocation provider instead of Google...
defaultPref("geo.provider.network.url", "https://api.beacondb.net/v1/geolocate");

/* Enable global toggles for muting the camera/microphone in WebRTC
  https://searchfox.org/mozilla-central/source/browser/app/profile/firefox.js#2452
  I don't use WebRTC anyway */
defaultPref("privacy.webrtc.globalMuteToggles", true); // [HIDDEN - Android]

/* Warn users when attempting to switch tabs in a window being shared over WebRTC
   https://searchfox.org/mozilla-central/source/browser/app/profile/firefox.js#2459 */
defaultPref("privacy.webrtc.sharedTabWarning", true); // [HIDDEN - Android/Thunderbird]

/* Enable Add-on Distribution Control (Install Origins)
Allows extensions to only be installed from websites they specify in their manifest
https://groups.google.com/g/firefox-dev/c/U7GpHE4R-ZY
https://searchfox.org/mozilla-central/source/toolkit/mozapps/extensions/internal/XPIDatabase.sys.mjs#403 */
defaultPref("extensions.install_origins.enabled", true);

// Harden CSP policy
// Currently disables WebAssembly (WASM) & upgrades insecure requests
defaultPref("extensions.webextensions.base-content-security-policy", "script-src 'self' https://* http://localhost:* http://127.0.0.1:* moz-extension: blob: filesystem: 'unsafe-eval' 'unsafe-inline'; upgrade-insecure-requests;");
defaultPref("extensions.webextensions.base-content-security-policy.v3", "script-src 'self'; upgrade-insecure-requests;");
defaultPref("extensions.webextensions.default-content-security-policy", "script-src 'self'; upgrade-insecure-requests;");
// defaultPref("extensions.webextensions.default-content-security-policy.v3", "script-src 'self'; upgrade-insecure-requests;"); // [DEFAULT]

// Strip tracking parameters from URLs when shared by default
defaultPref("privacy.query_stripping.strip_on_share.enabled", true); // [DEFAULT - non-Android/Thunderbird]

// Enable Smartblock Embeds/Placeholders
// Makes certain resources click to load
defaultPref("extensions.webcompat.smartblockEmbeds.enabled", true); // [DEFAULT - Nightly, HIDDEN - Android/Thunderbird]

/* Enable Cookies Having Independent Partitioned State (CHIPS)
This allows websites to set cookies with a 'Partitioned' attribute, meaning they're limited in scope
We still use ETP Strict for partioning anyways, so this could be useful as a defense in depth if a user decides to allow a specific domain (or domains) to access a third party cookie
https://developer.mozilla.org/docs/Web/Privacy/Privacy_sandbox/Partitioned_cookies
https://developer.mozilla.org/docs/Web/HTTP/Headers/Set-Cookie#partitioned
https://github.com/privacycg/CHIPS */
defaultPref("network.cookie.CHIPS.enabled", true); // [DEFAULT - Nightly]

// Disable Windows SSO
// https://support.mozilla.org/kb/windows-sso
defaultPref("network.http.windows-sso.container-enabled.0", false);
// defaultPref("network.http.windows-sso.enabled", false); // [DEFAULT]

// Disable Microsoft Entra
// https://www.microsoft.com/security/business/identity-access/microsoft-entra-single-sign-on
defaultPref("network.http.microsoft-entra-sso.container-enabled.0", false);
defaultPref("network.http.microsoft-entra-sso.enabled", false); // [DEFAULT]
defaultPref("network.microsoft-sso-authority-list", ""); // DEFENSE IN DEPTH

/* Prevent using Negotiate authentication by default
  This is modified by ex. RedHat/Fedora
  https://people.redhat.com/mikeb/negotiate/
  In fedora is "https://" for example */
defaultPref("network.negotiate-auth.trusted-uris", ""); // [DEFAULT]

// Enforce crashing on insecure password input
// defaultPref("intl.allow-insecure-text-input", false); // [DEFAULT, HIDDEN - non-Nightly]

// Always protect against MIME Exploits
// https://www.pcmag.com/encyclopedia/term/mime-exploit
defaultPref("security.block_fileuri_script_with_wrong_mime", true);
// defaultPref("security.block_Worker_with_wrong_mime", true); // [DEFAULT]

/* Sync with Remote Settings hourly, rather than the default of only once a day
This is used for delivering lots of security-critical databases (Ex. CRLite/revocation checks, malicious add-on blocklists, etc...)
So let's make sure our users are up to date as quick as possible */
defaultPref("services.settings.poll_interval", 3600);

/* ----------- LIBREWOLF & CUSTOM PART -----------*/
// List of custom DOH providers
// For more providers: https://github.com/curl/curl/wiki/DNS-over-HTTPS
defaultPref("doh-rollout.provider-list", '[{"UIName":"Quad9 - Real-time Malware Protection","uri":"https://dns.quad9.net/dns-query"},{"UIName":"DNS0 (ZERO) - Hardened Real-time Malware Protection","uri":"https://zero.dns0.eu"},{"UIName":"DNS0 - Real-time Malware Protection","uri":"https://dns0.eu"},{"UIName":"Mullvad - Ad/Tracking/Limited Malware Protection","uri":"https://base.dns.mullvad.net/dns-query"},{"UIName":"AdGuard (Public) - Ad/Tracking Protection","uri":"https://dns.adguard-dns.com/dns-query"},{"UIName":"Mullvad - No Filtering","uri":"https://dns.mullvad.net/dns-query"},{"UIName":"Wikimedia - No Filtering","uri":"https://wikimedia-dns.org/dns-query"},{"UIName":"AdGuard (Public) - No Filtering","uri":"https://unfiltered.adguard-dns.com/dns-query"},{"UIName":"DNS0 - Kids","uri":"https://kids.dns0.eu"},{"UIName":"Mullvad - Family","uri":"https://family.dns.mullvad.net/dns-query"},{"UIName":"AdGuard (Public) - Family Protection","uri":"https://family.adguard-dns.com/dns-query"},{"UIName":"Mullvad - Ad/Tracking/Limited Malware/Social Media Protection","uri":"https://extended.dns.mullvad.net/dns-query"},{"UIName":"Mullvad - Ad/Tracking/Limited Malware/Social Media/Adult/Gambling Protection","uri":"https://all.dns.mullvad.net/dns-query"},{"UIName":"LibreDNS (No Filtering)","uri":"https://doh.libredns.gr/dns-query"},{"UIName":"LibreDNS (Adblocking)","uri":"https://doh.libredns.gr/noads"},{"UIName":"DNS4All (No Filtering)","uri":"https://doh.dns4all.eu/dns-query"}]');

/* Default Doh Provider
  [NOTE]: In case the DNS provider set in this preference is DIFFERENT of one of the ones in the setting before
  then it will default to the first one in the doh-rollout.provider-list */
defaultPref("network.trr.default_provider_uri", "https://zero.dns0.eu") // Defaults to ZERO EU DNS

/* NO AI by default
https://codeberg.org/librewolf/issues/issues/1919 */

defaultPref("browser.ml.enable", false);
defaultPref("browser.ml.chat.enabled", false);
// Hide from UI
defaultPref("browser.ml.chat.hideFromLabs", true);
defaultPref("browser.ml.chat.hideLabsShortcuts", true);

// remove annoying ui elements from the about pages, including about:protections
defaultPref("browser.contentblocking.report.lockwise.enabled", false);
lockPref("browser.contentblocking.report.hide_vpn_banner", true);
lockPref("browser.contentblocking.report.vpn.enabled", false);
lockPref("browser.contentblocking.report.show_mobile_app", false);
lockPref("browser.vpn_promo.enabled", false);
lockPref("browser.promo.focus.enabled", false);
// ...about:addons recommendations sections and more
defaultPref("extensions.htmlaboutaddons.recommendations.enabled", false);
defaultPref("extensions.getAddons.showPane", false);
defaultPref("lightweightThemes.getMoreURL", ""); // disable button to get more themes
// ...about:preferences#home
defaultPref("browser.topsites.useRemoteSetting", false); // hide sponsored shortcuts button
// ...and about:config
defaultPref("browser.aboutConfig.showWarning", false);
// hide about:preferences#moreFromMozilla
defaultPref("browser.preferences.moreFromMozilla", false);

// hide pxi ads on toolbar firefox sync button
lockPref("identity.fxaccounts.toolbar.pxiToolbarEnabled", false);
// remove the "send to device" tab by default from opt-in sidebar layout
defaultPref("sidebar.main.tools", "history");
