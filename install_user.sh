#!/bin/sh

# User.js installation script
# This script is a work-in-progress installer. Use with caution.
# The author is not responsible for any data loss that may occur.
# Always verify the contents of any script downloaded from the internet.
# License: MPL v2.

# Define some colors for output
RED="\e[31m"
YELLOW="\e[33m"
CYAN="\e[96m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

# Function to check if the script is running as root
check_root() {
    if [ "${EUID:-"$(id -u)"}" -eq 0 ]; then
        echo -e "${RED}You shouldn't run this with elevated privileges (such as with doas/sudo).\nAborting...${ENDCOLOR}"
        exit 1
    fi
}

# Function to prompt the user to proceed
prompt_proceed() {
    echo -e "${YELLOW}Are you sure you want to proceed with the installation? (Yes/No)${ENDCOLOR}"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) break;;
            No ) echo -e "${GREEN}Exiting installer, no action performed.${ENDCOLOR}"; exit;;
        esac
    done
}

# Function to perform the installation
perform_install() {
    # Kill any running instances of Firefox
    pkill firefox

    # Warn the user about data loss and prompt for backup
    echo -e "${RED}This will delete all your Firefox data (history, bookmarks, etc.). Make sure to backup your data before proceeding.${ENDCOLOR}"
    echo -e "${YELLOW}Do you want to create a backup before proceeding? (Yes/No)${ENDCOLOR}"
    select backup in "Yes" "No"; do
        case $backup in
            Yes )
                # Create a backup directory if it doesn't exist
                mkdir -p $HOME/.mozilla/firefox_backup
                cp -r $HOME/.mozilla/firefox/*.default-release/* $HOME/.mozilla/firefox_backup/
                echo -e "${GREEN}Backup created at $HOME/.mozilla/firefox_backup.${ENDCOLOR}"
                break;;
            No ) break;;
        esac
    done

    # Delete all files in the default Firefox profile directory
    rm -rf $HOME/.mozilla/firefox/*.default-release/*

    # Copy the user.js configuration file to the Firefox profile directory
    cp user.js $HOME/.mozilla/firefox/*.default-release/

    # Or uncomment the following line if you want to set prefs.js instead of user.js
    # cp user.js $HOME/.mozilla/firefox/*.default-release/prefs.js

    echo -e "${GREEN}Installed configurations correctly. Bye!${ENDCOLOR}"
}

# Main script execution
check_root
echo -e "${YELLOW}Installing the user.js will reset all Firefox data.${ENDCOLOR}"
echo -e "${YELLOW}This will make you lose all your data (history, bookmarks, etc.).${ENDCOLOR}"
prompt_proceed
echo -e "${YELLOW}Ok, continuing with installation...${ENDCOLOR}"
perform_install
