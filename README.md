<h1 style="text-align: center;">Bennu: Firefox Configs </h1>

This is my own custom prefs.js and other adjustements for [Mozilla Firefox Browser](https://www.mozilla.org/en-US/firefox/browsers/) aimed to enhance the privacy and the security of the default settings of the browser.

Mainly, it is a personal configuration file created thanks to the configurations of the following proyects:

 - [Arkenfox user.js](https://github.com/arkenfox/user.js) custom user.js config file focus in privacy
 - [BetterFox.js](https://github.com/yokoffing/BetterFox) where there are 4 diferent custom user.js types 
 - [Phoenix](https://codeberg.org/celenity/Phoenix) a suite of configurations & advanced modifications for Mozilla Firefox, designed to put the user first - with a focus on privacy, security, freedom, & usability.
 - Some configs from [Librewolf](https://codeberg.org/librewolf) browser, a fork of Firefox focused on privacy, security and freedom.

From them I choose the best suitable options I think I consider most important for privacy concerns and take it into this prefs.js **For this reason take in mind that all credits for all the research and customization of their user.js goes to them, I just recopile them and put it together for usage**

Please use this configuration with the most up-to-date version of firefox for better matching. Currently is not developed to be compatible with ESR and Nightly builds.

### Little background

Nowdays is well known that big-company-driven browsers are following the dark path of AI and a ton of features that are not user focused. That is because some concerned users are creating software that go agains this crazy idea. This proyect is one of them.

I am not still a good script-writter so I am beginning to write my own automated scripts but meanwhile I write manuals as this for myself to learn, remember me and show what discoveries I made.

Also [Phoenix](https://codeberg.org/celenity/Phoenix) for some distributions the already configured scripts are avaiable and you could consider their aproach. I use arch (btw) but I don't install sudo nor doas or other privileged programs so I can't use AUR and thus i need to do all myself by hand. That's why I am creating this, for me, to kinda a document where document all the posible configs I found or do to my personal firefox configs.

## Important considerations

Although I reject big companies, I believe that if someone creates someone that does more good than harm it should be taken into accout. Therefore, I enable SafeBrowsing (related to Google and its telemetry). Arkenfox user.js ensure that:

*SafeBrowsing has taken many steps to preserve privacy. If required, a full url is never sent to Google, only a part-hash of the prefix, hidden with noise of other real part-hashes. Firefox takes measures such as stripping out identifying parameters and since SafeBrowsing v4 (FF57+) doesn't even use cookies. (#Turn on `browser.safebrowsing.debug` to monitor this activity)*

See: 
- [How Safe Browsing works in Firefox](https://feeding.cloud.geek.nz/posts/how-safe-browsing-works-in-firefox/)
- [Security/Safe Browsing](https://wiki.mozilla.org/Security/Safe_Browsing)
- [How does built-in Phishing and Malware Protection work?](https://support.mozilla.org/kb/how-does-phishing-and-malware-protection-work)
- [Can we make Safe Browsing safer?](https://educatedguesswork.org/posts/safe-browsing-privacy/)

As well as this, there may be settings that the more discerning may consider to be telemetry or that should be removed, however I consider a high rate of privacy along with network security rather than just privacy. If desired, anyone can modify the file to their liking.

The arkenfox config gives usage of both FPP (fingerprintingProtection) and RFP (resistFingerprinting) but due to the latter being more robust and strong, I'll use that one.
In case of needed FPP please consider using the preferences of the arkenfox config in a override.js file.

Also take care of the time that `arkenfox user.js`: it could be slow in updates so you should think about that from time to time and check whether or not your last config file was correct for you some versions later.

## Main Steps

It is always wise to rebuld all the configuration in your firefox. For that, please before proceeding nothing you should do a backup!

- Disclamer: **USE THIS GUIDE AS YOUR OWN RISK**: read all the documentation for both the file and the projects from which this configuration comes from. Remember this is only my own chooses for a config file and some advices.

After taking a backup, I always remove the main mozilla folder from my computer. The paths are different depending on the OS. This are some posible paths:

| OS                         | Path                                                                   |
| -------------------------- | -----------------------------------------------------------------------|
| Windows 7                  | `%APPDATA%\Mozilla\Firefox\Profiles\...`                               |
| Linux                      | `~/.mozilla/firefox/XXXXXXXX.your_profile_name/...`                    |
| OS X                       | `~/Library/Application Support/Firefox/Profiles/...`                   |
| Android                    | `/data/data/org.mozilla.firefox/files/mozilla/...`                     |
| Sailfish OS + Alien Dalvik | `/opt/alien/data/data/org.mozilla.firefox/files/mozilla/...`           |
| Windows (portable)         | `[firefox directory]\Data\profile\`                                    |

For example my `install.sh` script removes the profile directory from `~/.mozilla/firefox/*` and put in it the `user.js` but you could do this manually.

### 1. Download main files

I recommend this next files:

- `filter.pac` from [Phoenix](https://codeberg.org/celenity/Phoenix) proyect.
- `user.js` file from this repo. This file is configured to be as much compatible with arkenfox one and Phoenix configs as posible meanwhile maintaning my configs.
- `policies.json` from [Phoenix](https://codeberg.org/celenity/Phoenix) proyect, [Librewolf](https://codeberg.org/librewolf/settings/src/branch/master/distribution/policies.json) or my own. **Please read this [Policy Templates from Mozilla](https://mozilla.github.io/policy-templates/) because the location depends on the OS**
- `bennu.cfg` config file. See [Firefox autoconfig](https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig)

This selection is based on the order of the PKGBUILD in the AUR repository. See [phoenix-arch](https://aur.archlinux.org/packages/phoenix-arch).

### 2. Customize

Read and customize the mentioned and given files to your liking, remember this are more or less configured to be a very harden profile of Firefox and could be incompatible with some requirements.

Also remember thar you can use other `override-user.js` files to modify the existing ones without modifying them.

I try to make the configs as simple as possible so it is very probable that you will need to recheck the final configs in case something is missing or in case you'd like to modify something.

With this configs I install also Ublock Origin with the default config from the proyect and nothing else more!

### 3. Install files

1. First you should delete all the files inside the default profile of firefox you are using or create one new.
2. Install the modified `user.js` or `prefs.js` in the correct directory of your OS. See the table above, inside the `*.default-release` or the selected profile of firefox. To check what profile you are using, you could write `about:profiles` in the firefox search bar.
3. Install `policies.json` file in the correct directory of your OS. For example in my case because of Linux, it should be `firefox/distribution`. Please read [Policy Templates from Mozilla](https://mozilla.github.io/policy-templates/).
4. Also locate the folder `defaults/pref` in the `firefox` directory along side the `bennu.cfg` file. Check [Firefox Autoconfig](https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig) to check if everything is well put.
5. Finally, install the `filter.pac` file. For that, inside Firefox go to: `Settings > General > Network Settings > Automatic proxy configuration URL` and select the file. You should locate the file wherever you know that it will not be erased because the file will be read each time from that location so be careful with it. To know if is installed at the end of the `filter.pac` file exists this section:
```js
///////////////////////////////////////////////////////////////////////////////
//
// This line is just for testing; you can ignore it.  But, if you are having
// problems where you think this PAC file isn't being loaded, then change this
// to read "if (1)" and the alert box should appear when the browser loads this
// file.
//
// This works for IE4, IE5, IE5.5, IE6 and Netscape 2.x, 3.x, and 4.x.
// (For IE6, tested on Win2K)
// This does not work for Mozilla before 1.4 (and not for Netscape 6.x).
// In Mozilla 1.4+ and Fireox, this will write to the JavaScript console.
//
if (0) {
    alert("no-ads.pac: LOADED:\n" +
        "	version:	" + noadsver + "\n" +
        "	blackhole:	" + blackhole + "\n" +
        "	normal:		" + normal + "\n" +
        "	localproxy:	" + localproxy + "\n" +
        "	bypass:		" + bypass + "\n"
        //MSG
    );
}
```
Change `if(0) -> if(1)` and reload firefox. Opening the terminal with `Ctrl + Shift + J` you should see the alert notifying the version and all the details.

## Extensions

After using this configs I should advice to use some extensions to enhace even more the privacy and complete the setup for better personal usage by going through the Settings UI. I recommmend the next extensions for an even enhanced Firefox:

### Recomended

- [Ublock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) is installed by default with the `policies.json` file (we specified explicitly that we want it installed). Now I'd be wise to add the following list of add blockers to your Ublock (for more information see [add filter list Ublock](https://github.com/gorhill/ublock/wiki/Dashboard:-Filter-lists)):
   - [StevenBlack/Hosts](https://github.com/StevenBlack/hosts): This repository consolidates several reputable hosts files, and merges them into a unified hosts file with duplicates removed.
   - [Actually Legitimate URL Shortener Tool](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt) this removes unnecessary trackers from URLs. [Privacy Essentials](https://github.com/yokoffing/filterlists/blob/main/privacy_essentials.txt) and [Annoyance List](https://github.com/yokoffing/filterlists/blob/main/annoyance_list.txt) from Yokoffing (Betterfox).
   - [ClearURLs for uBo](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ClearURLs%20for%20uBo/clear_urls_uboified.txt) This makes 'unnecesary' the usage of the ClearURLs extension. This is because the number of extensions installed in a browser can be spoofed, so I'd be wise to have the less possible number of extensions.
   - [Hagezi Dns blocklist](https://github.com/hagezi/dns-blocklists) With the best filter being:
      - [Ultimate Pro Mini](https://cdn.jsdelivr.net/gh/hagezi/dns-blocklists@latest/adblock/ultimate.mini.txt)
      - [Fake List](https://cdn.jsdelivr.net/gh/hagezi/dns-blocklists@latest/adblock/fake.txt)
      - [Thread Feeds List](https://cdn.jsdelivr.net/gh/hagezi/dns-blocklists@latest/adblock/tif.medium.txt)
      - [Dynamic DNS blocking](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/adblock/dyndns.txt)
      - [Badware Hosted Blocking](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/adblock/hoster.txt)
      - [Most Abused TLDs](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/adblock/spam-tlds-ublock.txt)
      - [!Gambling](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/adblock/gambling.txt)
    - [1Hosts](https://o0.pages.dev/)
    - [AMP Hosts](https://www.github.developerdan.com/hosts/) Developer Dan's Hosts
    - [Adguard Ublock List](https://sebsauvage.net/hosts/hosts-adguard)
    - [BadBlock](https://badblock.celenity.dev/abp/amazon.txt) from the developer that created Phoenix
- [No Script](https://addons.mozilla.org/en-US/firefox/addon/noscript/) gives you the best available protection on the web. It allows JavaScript and other executable content to run only from trusted domains of your choice (e.g. your banking site), thus mitigating remotely exploitable vulnerabilities, such as Spectre and Meltdown.

#### Optional
- [Local CDN](https://www.localcdn.org/) LocalCDN protection. Even though there is people that said these are not that useful I'd like to have it.

### Licenses

This proyect is license under the Mozilla Public v2 License. See LICENSE.

Other proyects related are licensed under the Mozilla Public License Version 2.0 and GNU GENERAL PUBLIC LICENSE Version 3.
